package fr.pajonti.sejuapi.dto.tournament;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class TournamentRegistrationParametersV5 implements RiotDTO {
    private String name;
    private Integer providerId;
}

