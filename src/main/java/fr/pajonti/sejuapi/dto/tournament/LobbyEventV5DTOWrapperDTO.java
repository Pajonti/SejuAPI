package fr.pajonti.sejuapi.dto.tournament;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class LobbyEventV5DTOWrapperDTO implements RiotDTO {
    private List<LobbyEventV5DTO> eventList;
}

