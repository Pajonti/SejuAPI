package fr.pajonti.sejuapi.dto.tournament;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class LobbyEventV5DTO implements RiotDTO {
    /**
     * Timestamp from the event
     */
    private String timestamp;
    /**
     * The type of event that was triggered
     */
    private String eventType;
    /**
     * The puuid that triggered the event (Encrypted)
     */
    private String puuid;
}

