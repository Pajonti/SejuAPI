package fr.pajonti.sejuapi.dto.tournament;

import fr.pajonti.sejuapi.dto.RiotDTO;
import fr.pajonti.sejuapi.enums.league.MapType;
import fr.pajonti.sejuapi.enums.league.PickType;
import fr.pajonti.sejuapi.enums.shared.Region;
import lombok.Data;

import java.util.Set;

@Data
public class TournamentCodeV5DTO implements RiotDTO {
    /**
     * The tournament code.
     */
    private String code;
    /**
     * The spectator mode for the tournament code game.
     */
    private String spectators;
    /**
     * The lobby name for the tournament code game.
     */
    private String lobbyName;
    /**
     * The metadata for tournament code.
     */
    private String metaData;
    /**
     * The password for the tournament code game.
     */
    private String password;
    /**
     * The team size for the tournament code game.
     */
    private int teamSize;
    /**
     * The provider's ID.
     */
    private int providerId;
    /**
     * The pick mode for tournament code game.
     */
    private PickType pickType;
    /**
     * The tournament's ID.
     */
    private String tournamentId;
    /**
     * The tournament code's ID.
     */
    private String id;
    /**
     * The tournament code's region. (Legal values: BR, EUNE, EUW, JP, LAN, LAS, NA, OCE, PBE, RU, TR, KR)
     */
    private Region region;
    /**
     * The game map for the tournament code game
     */
    private MapType map;
    /**
     * The puuids of the participants (Encrypted)
     */
    private Set<String> participants;
}

