package fr.pajonti.sejuapi.dto.tournamentstub;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.Set;

@Data
public class TournamentCodeParametersV5DTO implements RiotDTO {
    private Set<String> allowedParticipants;
    private String metadata;
    private Integer teamSize;
    private String pickType;
    private String mapType;
    private String spectatorType;
    private Boolean enoughPlayers;
}

