package fr.pajonti.sejuapi.dto.tournamentstub;

import fr.pajonti.sejuapi.dto.RiotDTO;
import fr.pajonti.sejuapi.enums.shared.Region;
import lombok.Data;

@Data
public class ProviderRegistrationParametersV5DTO implements RiotDTO {
    /**
     * The region in which the provider will be running tournaments. (Legal values: BR, EUNE, EUW, JP, LAN, LAS, NA,
     * OCE, PBE, RU, TR, KR)
     */
    private Region region;
    /**
     * The provider's callback URL to which tournament game results in this region should be posted. The URL must be
     * well-formed, use the http or https protocol, and use the default port for the protocol (http URLs must use port
     * 80, https URLs must use port 443).
     */
    private String url;
}

