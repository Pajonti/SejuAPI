package fr.pajonti.sejuapi.dto.tournamentstub;

import fr.pajonti.sejuapi.dto.RiotDTO;
import fr.pajonti.sejuapi.enums.shared.Region;
import lombok.Data;

@Data
public class TournamentRegistrationParametersV5 implements RiotDTO {
    private String name;
    private Integer providerId;
}

