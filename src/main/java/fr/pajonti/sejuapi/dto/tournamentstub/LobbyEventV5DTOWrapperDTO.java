package fr.pajonti.sejuapi.dto.tournamentstub;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class LobbyEventV5DTOWrapperDTO implements RiotDTO {
    private List<LobbyEventV5DTO> eventList;
}

