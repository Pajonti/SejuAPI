package fr.pajonti.sejuapi.dto.valranked;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class PlayerDTO implements RiotDTO {
    /**
     * This field may be omitted if the player has been anonymized.
     */
    private String puuid;
    /**
     * This field may be omitted if the player has been anonymized.
     */
    private String gameName;
    /**
     * This field may be omitted if the player has been anonymized.
     */
    private String tagLine;
    private Long leaderboardRank;
    private Long rankedRating;
    private Long numberOfWins;
}


