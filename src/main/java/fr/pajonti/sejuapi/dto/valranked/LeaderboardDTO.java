package fr.pajonti.sejuapi.dto.valranked;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class LeaderboardDTO implements RiotDTO {
    /**
     * The shard for the given leaderboard.
     */
    private String shard;
    /**
     * The act id for the given leaderboard. Act ids can be found using the val-content API.
     */
    private String actId;
    /**
     * The total number of players in the leaderboard.
     */
    private Long totalPlayers;
    private List<PlayerDTO> players;
}


