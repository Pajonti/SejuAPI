package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class AbilityCastsDTO implements RiotDTO {
    private Integer grenadeCasts;
    private Integer ability1Casts;
    private Integer ability2Casts;
    private Integer ultimateCasts;
}


