package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class KillDTO implements RiotDTO {
    private Integer timeSinceGameStartMillis;
    private Integer timeSinceRoundStartMillis;
    /**
     * PUUID
     */
    private String killer;
    /**
     * PUUID
     */
    private String victim;
    private LocationDTO victimLocation;
    /**
     * List of PUUIDs
     */
    private List<String> assistants;
    private List<PlayerLocationsDTO> playerLocations;
    private FinishingDamageDTO finishingDamage;
}


