package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class TeamDTO implements RiotDTO {
    /**
     * This is an arbitrary string. Red and Blue in bomb modes. The puuid of the player in deathmatch.
     */
    private String teamId;
    private Boolean won;
    private Integer roundsPlayed;
    private Integer roundsWon;
    /**
     * Team points scored. Number of kills in deathmatch.
     */
    private Integer numPoints;
}


