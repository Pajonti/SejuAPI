package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class LocationDTO implements RiotDTO {
    private Integer x;
    private Integer y;
}


