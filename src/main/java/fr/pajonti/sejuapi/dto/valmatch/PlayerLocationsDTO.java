package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class PlayerLocationsDTO implements RiotDTO {
    private String puuid;
    private Float viewRadians;
    private LocationDTO location;
}


