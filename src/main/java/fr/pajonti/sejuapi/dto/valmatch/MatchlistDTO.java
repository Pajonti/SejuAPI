package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class MatchlistDTO implements RiotDTO {
    private String puuid;
    private List<MatchlistEntryDTO> history;
}


