package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;
import java.util.List;


@Data
public class RecentMatchesDTO implements RiotDTO {
    private Long currentTime;
    private List<String> matchIds;
}


