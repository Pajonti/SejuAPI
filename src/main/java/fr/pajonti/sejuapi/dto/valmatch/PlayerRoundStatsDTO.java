package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class PlayerRoundStatsDTO implements RiotDTO {
    private String puuid;
    private List<KillDTO> kills;
    private List<DamageDTO> damage;
    private Integer score;
    private EconomyDTO economy;
    private AbilityDTO ability;
}


