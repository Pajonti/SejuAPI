package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class MatchlistEntryDTO implements RiotDTO {
    private String matchId;
    private Long gameStartTimeMillis;
    private String queueId;
}


