package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class FinishingDamageDTO implements RiotDTO {
    private String damageType;
    private String damageItem;
    private Boolean isSecondaryFireMode;
}


