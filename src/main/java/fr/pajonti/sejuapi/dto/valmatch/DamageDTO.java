package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class DamageDTO implements RiotDTO {
    /**
     * PUUID
     */
    private String receiver;
    private Integer damage;
    private Integer legshots;
    private Integer bodyshots;
    private Integer headshots;
}


