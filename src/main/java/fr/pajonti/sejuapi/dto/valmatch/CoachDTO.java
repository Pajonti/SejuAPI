package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class CoachDTO implements RiotDTO {
    private String puuid;
    private String teamId;
}


