package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class RoundResultDTO implements RiotDTO {
    private Integer roundNum;
    private String roundResult;
    private String roundCeremony;
    private String winningTeam;
    /**
     * PUUID of player
     */
    private String bombPlanter;
    /**
     * PUUID of player
     */
    private String bombDefuser;
    private Integer plantRoundTime;
    private List<PlayerLocationsDTO> plantPlayerLocations;
    private LocationDTO plantLocation;
    private String plantSite;
    private Integer defuseRoundTime;
    private List<PlayerLocationsDTO> defusePlayerLocations;
    private LocationDTO defuseLocation;
    private List<PlayerRoundStatsDTO> playerStats;
    private String roundResultCode;
}


