package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import fr.pajonti.sejuapi.dto.valcontent.LocalizedNamesDto;
import lombok.Data;

import java.util.List;

@Data
public class MatchDTO implements RiotDTO {
    private MatchInfoDTO matchInfo;
    private List<PlayerDTO> players;
    private List<CoachDTO> coaches;
    private List<TeamDTO> teams;
    private List<RoundResultDTO> roundResults;
}


