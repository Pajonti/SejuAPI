package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class PlayerStatsDTO implements RiotDTO {
    private Integer score;
    private Integer roundsPlayed;
    private Integer kills;
    private Integer deaths;
    private Integer assists;
    private Integer playtimeMillis;
    private AbilityCastsDTO abilityCasts;
}


