package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class MatchInfoDTO implements RiotDTO {
    private String matchId;
    private String mapid;
    private Integer gameLengthMillis;
    private Long gameStartMillis;
    private String provisioningFlowId;
    private Boolean isCompleted;
    private String customGameName;
    private String queueId;
    /**
     * Probably should be set as an Enum, but we don't have access to the endpoint to mine dataset so far, and docs aren't
     * explaining it either
     */
    private String gameMode;
    private Boolean isRanked;
    private String seasonId;
}


