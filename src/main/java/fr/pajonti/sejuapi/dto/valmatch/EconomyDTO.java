package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class EconomyDTO implements RiotDTO {
    private Integer loadoutValue;
    private String weapon;
    private String armor;
    private Integer remaining;
    private Integer spent;
}


