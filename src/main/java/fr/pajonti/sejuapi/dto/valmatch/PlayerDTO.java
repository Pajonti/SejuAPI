package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class PlayerDTO implements RiotDTO {
    private String puuid;
    private String gameName;
    private String tagLine;
    private String teamId;
    private String partyId;
    private String characterId;
    private PlayerStatsDTO stats;
    private Integer competitiveTier;
    private String playerCard;
    private String playerTitle;
}


