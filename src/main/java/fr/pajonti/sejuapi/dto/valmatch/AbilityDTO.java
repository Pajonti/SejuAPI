package fr.pajonti.sejuapi.dto.valmatch;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class AbilityDTO implements RiotDTO {
    private String grenadeEffects;
    private String ability1Effects;
    private String ability2Effects;
    private String ultimateEffects;
}


