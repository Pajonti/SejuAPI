package fr.pajonti.sejuapi.dto.valcontent;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.util.List;

@Data
public class ContentDTO implements RiotDTO {
    private String version;
    private List<ContentItemDTO> characters;
    private List<ContentItemDTO> maps;
    private List<ContentItemDTO> chromas;
    private List<ContentItemDTO> skins;
    private List<ContentItemDTO> skinLevels;
    private List<ContentItemDTO> equips;
    private List<ContentItemDTO> gameModes;
    private List<ContentItemDTO> sprays;
    private List<ContentItemDTO> sprayLevels;
    private List<ContentItemDTO> charms;
    private List<ContentItemDTO> charmLevels;
    private List<ContentItemDTO> playerCards;
    private List<ContentItemDTO> playerTitles;
    private List<ActDTO> acts;
}

