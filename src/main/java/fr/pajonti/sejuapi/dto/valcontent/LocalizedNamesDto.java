package fr.pajonti.sejuapi.dto.valcontent;

import com.google.gson.*;
import com.google.gson.annotations.JsonAdapter;
import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

import java.lang.reflect.Type;

@Data
@JsonAdapter(LocalizedNamesDto.Serializer.class)
public class LocalizedNamesDto implements RiotDTO {
    private String ar_AE;
    private String de_DE;
    private String en_GB;
    private String en_US;
    private String es_ES;
    private String es_MX;
    private String fr_FR;
    private String id_ID;
    private String it_IT;
    private String ja_JP;
    private String ko_KR;
    private String pl_PL;
    private String pt_BR;
    private String ru_RU;
    private String th_TH;
    private String tr_TR;
    private String vi_VN;
    private String zh_CN;
    private String zh_TW;

    static class Serializer implements JsonSerializer<LocalizedNamesDto>, JsonDeserializer<LocalizedNamesDto> {

        /**
         * We'd like to rely on GSon mechanisms to read elements, but unfortunately, those elements have a - in their name,
         * this making it impossible to declare them as a variable. So we have to fall back to a JSONObject behaviour
         * to parse a response
         * @param jsonElement The element provided as unmarshall element
         * @param type Type superinterface (Internal behaviour of GSon)
         * @param jsonDeserializationContext JsonDeserializationContext
         * @return Item parsed
         * @throws JsonParseException If JSOn is badly formatted
         */
        @Override
        public LocalizedNamesDto deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            LocalizedNamesDto dto = new LocalizedNamesDto();

            dto.setAr_AE(jsonObject.get("ar-AE") != null ? jsonObject.get("ar-AE").getAsString() : null);
            dto.setDe_DE(jsonObject.get("de-DE") != null ? jsonObject.get("de-DE").getAsString() : null);
            dto.setEn_GB(jsonObject.get("en-GB") != null ? jsonObject.get("en-GB").getAsString() : null);
            dto.setEn_US(jsonObject.get("en-US") != null ? jsonObject.get("en-US").getAsString() : null);
            dto.setEs_ES(jsonObject.get("es-ES") != null ? jsonObject.get("es-ES").getAsString() : null);
            dto.setEs_MX(jsonObject.get("es-MX") != null ? jsonObject.get("es-MX").getAsString() : null);
            dto.setFr_FR(jsonObject.get("fr-FR") != null ? jsonObject.get("fr-FR").getAsString() : null);
            dto.setId_ID(jsonObject.get("id-ID") != null ? jsonObject.get("id-ID").getAsString() : null);
            dto.setIt_IT(jsonObject.get("it-IT") != null ? jsonObject.get("it-IT").getAsString() : null);
            dto.setJa_JP(jsonObject.get("ja-JP") != null ? jsonObject.get("ja-JP").getAsString() : null);
            dto.setKo_KR(jsonObject.get("ko-KR") != null ? jsonObject.get("ko-KR").getAsString() : null);
            dto.setPl_PL(jsonObject.get("pl-PL") != null ? jsonObject.get("pl-PL").getAsString() : null);
            dto.setPt_BR(jsonObject.get("pt-BR") != null ? jsonObject.get("pt-BR").getAsString() : null);
            dto.setRu_RU(jsonObject.get("ru-RU") != null ? jsonObject.get("ru-RU").getAsString() : null);
            dto.setTh_TH(jsonObject.get("th-TH") != null ? jsonObject.get("th-TH").getAsString() : null);
            dto.setTr_TR(jsonObject.get("tr-TR") != null ? jsonObject.get("tr-TR").getAsString() : null);
            dto.setVi_VN(jsonObject.get("vi-VN") != null ? jsonObject.get("vi-VN").getAsString() : null);
            dto.setZh_CN(jsonObject.get("zh-CN") != null ? jsonObject.get("zh-CN").getAsString() : null);
            dto.setZh_TW(jsonObject.get("zh-TW") != null ? jsonObject.get("zh-TW").getAsString() : null);


            return dto;
        }

        @Override
        public JsonElement serialize(LocalizedNamesDto localizedNamesDto, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject object = new JsonObject();

            object.addProperty("ar-AE", localizedNamesDto.ar_AE);
            object.addProperty("de-DE", localizedNamesDto.de_DE);
            object.addProperty("en-GB", localizedNamesDto.en_GB);
            object.addProperty("en-US", localizedNamesDto.en_US);
            object.addProperty("es-ES", localizedNamesDto.es_ES);
            object.addProperty("es-MX", localizedNamesDto.es_MX);
            object.addProperty("fr-FR", localizedNamesDto.fr_FR);
            object.addProperty("id-ID", localizedNamesDto.id_ID);
            object.addProperty("it-IT", localizedNamesDto.it_IT);
            object.addProperty("ja-JP", localizedNamesDto.ja_JP);
            object.addProperty("ko-KR", localizedNamesDto.ko_KR);
            object.addProperty("pl-PL", localizedNamesDto.pl_PL);
            object.addProperty("pt-BR", localizedNamesDto.pt_BR);
            object.addProperty("ru-RU", localizedNamesDto.ru_RU);
            object.addProperty("th-TH", localizedNamesDto.th_TH);
            object.addProperty("tr-TR", localizedNamesDto.tr_TR);
            object.addProperty("vi-VN", localizedNamesDto.vi_VN);
            object.addProperty("zh-CN", localizedNamesDto.zh_CN);
            object.addProperty("zh-TW", localizedNamesDto.zh_TW);

            return object;
        }
    }
}

