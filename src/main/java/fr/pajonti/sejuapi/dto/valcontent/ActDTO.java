package fr.pajonti.sejuapi.dto.valcontent;

import fr.pajonti.sejuapi.dto.RiotDTO;
import lombok.Data;

@Data
public class ActDTO implements RiotDTO {
    private String name;
    private LocalizedNamesDto localizedNames;
    private String id;
    private String parentId;
    private Boolean isActive;
}


