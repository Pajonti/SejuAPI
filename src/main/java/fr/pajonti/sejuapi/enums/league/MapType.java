package fr.pajonti.sejuapi.enums.league;

/**
 * Enum for Maps, used in the TournamentStub and Tournament endpoints
 */
public enum MapType {

    /**
     * Summoners Rift Map (5vs5)
     */
    SUMMONERS_RIFT("SUMMONERS_RIFT"),
    /**
     * Howling Abyss Map (ARAM)
     */
    HOWLING_ABYSS("HOWLING_ABYSS");

    public final String textValue;

    MapType(String textValue) {
        this.textValue = textValue;
    }

    /**
     * Returns the enum textValue
     * @return Enum textValue
     */
    public String getValue(){
        return textValue;
    }

    /**
     * Returns the Enum object based on the passed value, or null if no enum is found
     * @param value Value to check enum against
     * @return Enum object based on the passed value, or null if no enum is found
     */
    public static MapType getEnum(String value){
        for (MapType p : values()) {
            if (p.textValue.equals(value)) {
                return p;
            }
        }
        return null;
    }
}
