package fr.pajonti.sejuapi.enums.league;

/**
 * Enum for Spectators, used in the TournamentStub and Tournament endpoints
 */
public enum SpectatorType {

    /**
     * No spectators allowed
     */
    NONE("NONE"),
    /**
     * Only spectators who joined the lobby can watch the game
     */
    LOBBYONLY("LOBBYONLY"),

    /**
     * Public viewership
     */
    ALL("ALL");

    public final String textValue;

    SpectatorType(String textValue) {
        this.textValue = textValue;
    }

    /**
     * Returns the enum textValue
     * @return Enum textValue
     */
    public String getValue(){
        return textValue;
    }

    /**
     * Returns the Enum object based on the passed value, or null if no enum is found
     * @param value Value to check enum against
     * @return Enum object based on the passed value, or null if no enum is found
     */
    public static SpectatorType getEnum(String value){
        for (SpectatorType p : values()) {
            if (p.textValue.equals(value)) {
                return p;
            }
        }
        return null;
    }
}
