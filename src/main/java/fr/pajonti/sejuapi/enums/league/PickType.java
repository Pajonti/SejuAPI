package fr.pajonti.sejuapi.enums.league;

/**
 * Enum for Pick modes, used in the TournamentStub and Tournament endpoints
 */
public enum PickType {

    /**
     * Blind Pick mode
     */
    BLIND_PICK("BLIND_PICK"),
    /**
     * Draft Pick mode
     */
    DRAFT_MODE("DRAFT_MODE"),
    /**
     * All Random Pick mode
     */
    ALL_RANDOM("ALL_RANDOM"),
    /**
     * Tournament Pick mode (3 bans - 3 picks - 2 bans - 2 picks)
     */
    TOURNAMENT_DRAFT("TOURNAMENT_DRAFT");

    public final String textValue;

    PickType(String textValue) {
        this.textValue = textValue;
    }

    /**
     * Returns the enum textValue
     * @return Enum textValue
     */
    public String getValue(){
        return textValue;
    }

    /**
     * Returns the Enum object based on the passed value, or null if no enum is found
     * @param value Value to check enum against
     * @return Enum object based on the passed value, or null if no enum is found
     */
    public static PickType getEnum(String value){
        for (PickType p : values()) {
            if (p.textValue.equals(value)) {
                return p;
            }
        }
        return null;
    }
}
