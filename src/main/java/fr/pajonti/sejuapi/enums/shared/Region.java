package fr.pajonti.sejuapi.enums.shared;

public enum Region {
    BR("BR"),
    EUNE("EUNE"),
    EUW("EUW"),
    JP("JP"),
    LAN("LAN"),
    LAS("LAS"),
    NA("NA"),
    OCE("OCE"),
    PBE("PBE"),
    RU("RU"),
    TR("TR"),
    KR("KR"),
    STUB("STUB");

    public final String region;

    Region(String region) {
        this.region = region;
    }

    /**
     * Returns the enum textValue
     * @return Enum textValue
     */
    public String getValue(){
        return region;
    }

    /**
     * Returns the Enum object based on the passed value, or null if no enum is found
     * @param value Value to check enum against
     * @return Enum object based on the passed value, or null if no enum is found
     */
    public static Region getEnum(String value){
        for (Region p : values()) {
            if (p.region.equals(value)) {
                return p;
            }
        }
        return null;
    }
}
